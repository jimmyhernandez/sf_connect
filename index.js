require('dotenv').config();
const nodemailer = require('nodemailer');
const nforce = require('nforce');
const express = require('express');
const path = require('path');
const app = express();
const PORT = process.env.PORT || 3400;
let _oauthsf = null;


let sfConnection = (req,resp,next) => {
  let connection = nforce.createConnection({
    clientId: process.env.CONSUMER_KEY,
    clientSecret: process.env.CONSUMER_SECRET,
    redirectUri: process.env.REDIRECT_URI,
    apiVersion: 'v42.0',
    environment: process.env.ENVIRONMENT
  });
  req.sfConn = connection;
  next();
};

let getSfOauth = (req,res,next) => {
  let sfcon = req.sfConn;

  sfcon.authenticate({ username: process.env.SF_USERNAME, password: process.env.SF_PASSWORD, securityToken: process.env.SF_TOKEN}, callback);
  
  function callback(err, resp){
    if(!err) {
      req.sfOauth = resp;
      next();
   }
  }
};

let sendEmail = () => {
    
  const transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'e57rfplpodn7d2on@ethereal.email',
        pass: '2sdYgc5n6vUxZ2kdPP'
    }
  });

  // Message object
  let message = {
    from: 'devacc201@gmail.com',
    to: 'devacc201@gmail.com',
    subject: 'Nodemailer is unicode friendly ✔',
    text: 'Hello to myself!',
    html: '<p><b>Hello</b> to myself!</p>'
  };

  transporter.sendMail(message, (err, info) => {
      if (err) {
          console.log('Error occurred. ' + err.message);
          return process.exit(1);
      }

      console.log('Message sent: %s', info.messageId);
      // Preview only available when sending through an Ethereal account
      console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
  });

};

let test = (req,resp,next) => {
  //console.log('Test');
  //console.log(req.sfConn);
  //console.log(req.sfOauth);
  resp.send(req.sfOauth);
  sendEmail();
  next();
}

app.get('/',[sfConnection,getSfOauth,test]);

app.use('/static', express.static(path.join(__dirname, 'public')));

app.listen(PORT, () => {
  console.log('Server running port: ' + PORT);
});